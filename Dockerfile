FROM openjdk:11.0.2-jre-stretch

WORKDIR /app
COPY ./target/demo*.jar .
RUN chmod a+rw demo*.jar

ENV ACTIVE_PROFILE="local"
ENV JVM_OPTS="-Xmx2048m"

RUN useradd -ms /bin/bash demo
USER demo
ENTRYPOINT exec java -Dspring.profiles.active=$ACTIVE_PROFILE $JVM_OPTS -ea -jar /app/demo*.jar