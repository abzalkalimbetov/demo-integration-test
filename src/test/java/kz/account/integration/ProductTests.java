package kz.account.integration;

import kz.account.dto.Product;
import kz.account.model.ProductEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;


class ProductTests extends WebIntegrationTest{

	@Test
	@Transactional
	void getProducts_GetList_SingleProductResponse() throws Exception {
		ProductEntity productEntity = createRandomProduct();

		String result = mockMvc.perform(
				MockMvcRequestBuilders.get("/products")
						.contentType(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value())).andReturn().getResponse().getContentAsString();

		List<Product> products = objectMapper.readValue(result, new TypeReference<List<Product>>(){});
		TestCase.assertEquals(products.size(), 1);
		TestCase.assertEquals(products.get(0).getName(), productEntity.getName());

	}

	@Test
	@Transactional
	public void saveProducts_CreateSimpleProduct_CreatedProductResponse() throws Exception {
		Product product = new Product().name("Bread").price(new BigDecimal(100));

		String result = mockMvc.perform(
				MockMvcRequestBuilders.post("/products")
						.content(objectMapper.writeValueAsString(product))
						.contentType(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value())).andReturn().getResponse().getContentAsString();

		Product expectedProduct = objectMapper.readValue(result, Product.class);
		TestCase.assertEquals(product.getName(), expectedProduct.getName());
		TestCase.assertEquals(product.getPrice(), expectedProduct.getPrice());
	}

	@Test
	@Transactional
	public void saveProducts_CreateProductWithEmptyName_BadRequestResponse() throws Exception {
		ProductEntity productEntity = new ProductEntity().setPrice(new BigDecimal(100));

		mockMvc.perform(
				MockMvcRequestBuilders.post("/products")
						.content(objectMapper.writeValueAsString(productEntity))
						.contentType(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().is(HttpStatus.BAD_REQUEST.value()));
	}

	@Test
	@Transactional
	public void saveProducts_CreateProductWithNegativePrice_BadRequestResponse() throws Exception {
		ProductEntity productEntity = new ProductEntity().setName("name").setPrice(new BigDecimal(-5));

		mockMvc.perform(
				MockMvcRequestBuilders.post("/products")
						.content(objectMapper.writeValueAsString(productEntity))
						.contentType(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().is(HttpStatus.BAD_REQUEST.value()));
	}

	/*@Test
	@Transactional
	public void updateProduct_UpdateProductName_UpdatedProductResponse() throws Exception {
		ProductEntity productEntity = createRandomProduct();

		productEntity.setName("Change it");
		mockMvc.perform(
				MockMvcRequestBuilders.put("/products/"+ productEntity.getId())
						.content(objectMapper.writeValueAsString(productEntity))
						.contentType(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value())).andReturn().getResponse().getContentAsString();
		String result = mockMvc.perform(
				MockMvcRequestBuilders.get("/products/"+ productEntity.getId())
						.contentType(MediaType.APPLICATION_JSON)
		).andExpect(MockMvcResultMatchers.status().is(HttpStatus.OK.value())).andReturn().getResponse().getContentAsString();



		ProductEntity expectedProductEntity = objectMapper.readValue(result, ProductEntity.class);

		TestCase.assertEquals(productEntity.getName(), expectedProductEntity.getName());
		TestCase.assertEquals(productEntity.getId(), expectedProductEntity.getId());


	}*/

}
