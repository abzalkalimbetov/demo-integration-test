package kz.account.integration;

import kz.account.model.ProductEntity;
import kz.account.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class WebIntegrationTest {
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected ProductRepository productRepository;

    public ProductEntity createRandomProduct() {
        ProductEntity productEntity = new ProductEntity()
                .setName("meat")
                .setPrice(new BigDecimal(1950));
        productEntity = productRepository.save(productEntity);
        return productEntity;
    }


}
