CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE product_entity (id uuid NOT NULL DEFAULT uuid_generate_v4(), name varchar(255), price numeric(19, 2), primary key (id));