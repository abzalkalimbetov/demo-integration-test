package kz.account.mapper;

import kz.account.dto.Product;
import kz.account.model.ProductEntity;

public class ProductMapper {

    public static Product toDto(ProductEntity productEntity) {
        return new Product()
                .id(productEntity.getId())
                .price(productEntity.getPrice())
                .name(productEntity.getName());
    }

    public static ProductEntity fromDto(Product product) {
        return new ProductEntity()
                .setName(product.getName())
                .setPrice(product.getPrice());
    }
}
