package kz.account.service;

import kz.account.dto.Product;
import kz.account.mapper.ProductMapper;
import kz.account.model.ProductEntity;
import kz.account.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional
    public ProductEntity getTransactional() {
        return null;
    }

    public Product getById(UUID id) {
        return ProductMapper.toDto(productRepository.findById(id).orElseThrow());
    }

    public Page<ProductEntity> getProducts(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    public List<Product> getProducts() {
        return productRepository.findAll().stream()
                .map(ProductMapper::toDto).collect(Collectors.toList());
    }

    public Product saveProduct(Product product) {
        return ProductMapper.toDto(productRepository.save(ProductMapper.fromDto(product)));
    }
}
